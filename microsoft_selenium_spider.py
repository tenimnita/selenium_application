import urlparse
from datetime import datetime
import time

from pyvirtualdisplay.display import Display

from goldmansach.spider_name_config import SPIDER_NAME_MICROSOFT
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import logging

from goldmansach.my_mongo import microsoft_tmp_collection, item_collection


def check_exist(id):
    existed_1 = microsoft_tmp_collection.find_one(filter={
        'id': id
    })
    # existed_2 = item_collection.find_one(filter={
    #     'site': SPIDER_NAME_MICROSOFT,
    #     'id': id
    # })
    return existed_1


if __name__ == '__main__':
    # Init logging
    logging.basicConfig(
        filename='log/microsoft/selenium/{}.log'.format(datetime.now().strftime('%Y_%m_%d_%H_%M')),
        level=logging.INFO, format='%(asctime)s %(message)s')
    logging.info('starting....')
    WAITING_WHEN_SEARCH = 6
    # Delete the tmp db
    # microsoft_tmp_collection.delete_many(filter={})
    # Init display
    display = Display(visible=0, size=(1024, 768))
    display.start()
    # Start chrome
    driver = webdriver.Chrome('./chromedriver')
    # driver = webdriver.PhantomJS('./bin/phantomjs')
    driver.set_window_size(1024, 768) # set browser size.
    driver.get('https://careers.microsoft.com/search.aspx')
    logging.info('Ready')
    # browser.find_element_by_id('ContentPlaceHolder1_srSearch_search_msdProduct_chkSelectAll').click()
    select_init = 1
    product_select = select_init
    driver.find_element_by_id('ContentPlaceHolder1_srSearch_search_close').click()
    driver.find_element_by_id('multiselect_Product').click()
    driver.find_element_by_id('ContentPlaceHolder1_srSearch_search_msdProduct_chkSelectAll').click()

    while product_select <= 86:
        logging.info(u'select product: {}'.format(product_select))
        # deselect
        if product_select > select_init:
            logging.info(u'deselect')
            driver.find_element_by_id('multiselect_Product').click()
            driver.find_element_by_id('chkIdProduct_{}'.format(product_select - 1)).click()
            time.sleep(1)

        select_title = driver.find_element_by_id('chkIdProduct_{}'.format(product_select)).get_attribute('title')
        logging.info(u'select: {}'.format(select_title))
        driver.find_element_by_id('chkIdProduct_{}'.format(product_select)).click()
        time.sleep(1)
        driver.find_element_by_id('ContentPlaceHolder1_srSearch_search_msdProduct_txtMultiSelect').click()

        # click search button
        driver.find_element_by_id('ContentPlaceHolder1_srSearch_search_btnSearch').click()
        time.sleep(WAITING_WHEN_SEARCH)

        # items = {}
        paging = 1
        n_insert = 0
        while True:
            # print 'finished waiting'
            logging.info(u'Paging = {}'.format(paging))
            has_new_item = False
            try:
                for search_node in driver.find_elements_by_xpath(
                        '//div[@id="tblSearchResults"]/div[contains(@class, "Result")]'):
                    url_node = search_node.find_element_by_xpath('.//a')
                    posted_date_node = search_node.find_element_by_xpath('./div[@class="grid-cell"][4]')
                    # print url_node.get_attribute('innerHTML')
                    # print posted_date_node.get_attribute('innerHTML')
                    # items[url_node.get_attribute("href")] = posted_date_node.text
                    url = url_node.get_attribute("href")
                    url = urlparse.urljoin(base='https://careers.microsoft.com/search.aspx', url=url)
                    parsed = urlparse.urlparse(url)
                    id = urlparse.parse_qs(parsed.query)['jid'][0]
                    posting_date = posted_date_node.text

                    if check_exist(id):
                        logging.info(u'existed: {}'.format(url))
                        continue
                    else:
                        n_insert += 1
                        has_new_item = True
                        microsoft_tmp_collection.insert_one({
                            'id': id,
                            'url': url,
                            'posting_date': posting_date
                        })
                        logging.info(u'inserted: {}'.format(url))
            except NoSuchElementException:
                logging.info(u'Can not find search results xpath')

            if has_new_item:
                # click next button
                try:
                    next_button = driver.find_element_by_xpath('//a[@class="commandNext"]')
                except NoSuchElementException:
                    logging.info(u'No next button, break this product now')
                    break
                if next_button.get_attribute("href"):
                    # time.sleep(3)
                    logging.info(u'click next button')
                    next_button.click()
                    # logging.info('wait 5 seconds')
                    time.sleep(WAITING_WHEN_SEARCH)
                    paging += 1
                else:
                    logging.info(u'Next button has no href, break this product now')
                    break
            else:
                break
        logging.info(u'finished product={}, total_insert={}'.format(select_title, n_insert))
        logging.info('--------------------------------------------')
        product_select += 1

    driver.quit()