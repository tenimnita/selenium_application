# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import datetime
import scrapy


class MyItem(scrapy.Item):
    crawled_time = scrapy.Field()
    id = scrapy.Field()
    url = scrapy.Field()
    site = scrapy.Field()
    posting_date_str = scrapy.Field()
    posting_date = scrapy.Field()
    title = scrapy.Field()
    location = scrapy.Field()
    full_text = scrapy.Field()

class GoldmansachItem(MyItem):
    site = scrapy.Field()
    group = scrapy.Field()
    experience = scrapy.Field()
    skills = scrapy.Field()
    division = scrapy.Field()
    time = scrapy.Field()


class MicrosoftItem(MyItem):
    site = scrapy.Field()
    job_code = scrapy.Field()
    job_family = scrapy.Field()
    job_product = scrapy.Field()
    division = scrapy.Field()