import pymongo
from pymongo.mongo_client import MongoClient
from goldmansach.spider_name_config import SPIDER_NAME_GOLDMANSACHS, SPIDER_NAME_MICROSOFT

client = MongoClient('localhost', 27017)  # default maxPoolSize = 100
db = client['vishrut']
#
# goldmansach_collection = db[SPIDER_NAME_GOLDMANSACHS]
# goldmansach_collection.create_index([
#     ('url', pymongo.ASCENDING),
# ], unique=True)

microsoft_tmp_collection = db[SPIDER_NAME_MICROSOFT + '_tmp']
microsoft_tmp_collection.create_index([
    ('id', pymongo.ASCENDING),
], unique=True)

# microsoft_collection = db[SPIDER_NAME_MICROSOFT]
# goldmansach_collection.create_index([
#     ('id', pymongo.ASCENDING),
# ], unique=True)


item_collection = db['items']

item_collection.create_index([
    ('site', pymongo.ASCENDING),
], unique=False)

item_collection.create_index([
    ('site', pymongo.ASCENDING),
    ('id', pymongo.ASCENDING),
], unique=True)