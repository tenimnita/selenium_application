# -*- coding: utf-8 -*-

import re
from datetime import datetime
from functools import partial
from urlparse import urljoin, urlparse, parse_qs
import scrapy
from scrapy.http import Request

from goldmansach import my_mongo, spider_name_config
from goldmansach.items import GoldmansachItem
from goldmansach.spider_name_config import SPIDER_NAME_GOLDMANSACHS


class GoldmansachSpider(scrapy.Spider):
    name = SPIDER_NAME_GOLDMANSACHS

    allowed_domains = ["www.goldmansachs.com"]

    start_urls = [
        'http://www.goldmansachs.com/a/data/jobs/americas.html',
        'http://www.goldmansachs.com/a/data/jobs/asia_except_japan.html',
        'http://www.goldmansachs.com/a/data/jobs/emea.html',
        'http://www.goldmansachs.com/a/data/jobs/india.html',
        'http://www.goldmansachs.com/a/data/jobs/japan.html'
    ]

    def __init__(self, *args, **kwargs):
        super(GoldmansachSpider, self).__init__(*args, **kwargs)
        self._failed_urls = []

    def parse_item(self, item, response):
        # item = GoldmansachItem()
        self.logger.info('parse item: {}'.format(response.url))

        item['crawled_time'] = datetime.now()
        item['site'] = SPIDER_NAME_GOLDMANSACHS

        main_content = []
        for node in response.xpath('//div[@id="content"]/section[@class="grid2-padded"]//article'):
            # print 'aaaaaaaaaaaaaaaaaaaa'
            in_content = False
            for content_node in node.xpath('./node()'):
                if (content_node.xpath('./a[@class="buttonLink"]').extract_first() != None):
                    if in_content == False:
                        # start of main content
                        in_content = True
                        continue
                    else:
                        # end of main content
                        break
                if in_content:
                    # print content_node.extract().strip()
                    main_content.append(content_node.extract().strip())
            break
        if item['posting_date_str']:
            item['posting_date'] = self.parse_datetime(item['posting_date_str'])
        item['full_text'] = main_content
        yield item

    def parse(self, response):
        self.logger.info('parse list: {}'.format(response.url))
        items = response.xpath('//table[contains(@class, "searchListing")]/tbody/tr')
        if len(items) == 0:
            self.logger.warning('No item URL from: {}'.format(response.url))
        for item in items:
            url = item.xpath('.//a/@href').extract_first()
            if (url):
                url = urljoin(response.url, url)
                id = self.parse_id(url)
                if id:
                    exist = my_mongo.item_collection.find_one(filter={
                        'site': SPIDER_NAME_GOLDMANSACHS,
                        'id': id
                    })
                    if not exist:
                        yield_item = GoldmansachItem(
                            id=id,
                            url=url,
                            title=item.xpath('.//a/text()').extract_first(),
                            division=item.xpath('./td[1]/text()').extract_first(),
                            location=item.xpath('./td[2]/text()').extract_first(),
                            posting_date_str=item.xpath('./td[3]/text()').extract_first(),
                            time=item.xpath('./td[4]/text()').extract_first(),
                        )
                        # print yield_item
                        yield Request(url=url, callback=partial(self.parse_item, yield_item))

    def parse_id(self, url):
        obj = re.match(r'.*/a/data/jobs/(\d+).html', url)
        if obj:
            return obj.group(1)
        else:
            return None

    def parse_datetime(self, datetime_str):
        try:
            datetime_object = datetime.strptime(datetime_str, '%Y-%m-%d')
            return datetime_object
        except:
            self.logger.info(u'Can not parse datetime {}'.format(datetime_str))
            return None
