# -*- coding: utf-8 -*-

import urlparse
import scrapy
from datetime import datetime

from goldmansach import my_mongo, settings
from goldmansach.items import MicrosoftItem
from goldmansach.spider_name_config import SPIDER_NAME_MICROSOFT


class MicrosoftSpider(scrapy.Spider):
    name = SPIDER_NAME_MICROSOFT

    allowed_domains = ["careers.microsoft.com"]

    start_urls = []

    def __init__(self, *args, **kwargs):
        settings.LOG_FILE = 'log/microsoft/' + datetime.now().strftime('%Y-%m-%d-%H-%M') + '.log'
        super(MicrosoftSpider, self).__init__(*args, **kwargs)
        inserted = 0
        for item in my_mongo.microsoft_tmp_collection.find(filter={}):  # .limit(10)
            existed = my_mongo.item_collection.find_one(filter={
                'site': SPIDER_NAME_MICROSOFT,
                'id': item['id']
            })
            if not existed:
                inserted += 1
                self.start_urls.append(item['url'].replace('JobDetails', 'jobdetails'))
        self.logger.info(u'Start with {} urls'.format(inserted))

    def parse(self, response):
        self.logger.info('parse item: {}'.format(response.url))

        parsed = urlparse.urlparse(response.url)
        id = urlparse.parse_qs(parsed.query)['jid'][0]

        item = MicrosoftItem()
        item['url'] = response.url
        item['id'] = id
        item['site'] = SPIDER_NAME_MICROSOFT
        item['crawled_time'] = datetime.now()
        existed = my_mongo.item_collection.find_one(filter={
            'site': SPIDER_NAME_MICROSOFT,
            'id': id
        })
        if existed:
            self.logger.info(u'Exited ID when scraping item from: {}'.format(response.url))
            return
        # else:
        #     self.logger.info(u'Delete tmp collection id={}'.format(id))
        #     my_mongo.microsoft_tmp_collection.delete_one(filter={
        #         'id': id
        #     })
        item['title'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblJobTitleText"]/text()').extract_first()
        item['full_text'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblDescription"]/node()').extract()
        item['location'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblLocationText"]/text()').extract_first()
        item['job_family'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblJobCategoryText"]/text()').extract_first()
        item['job_product'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblJobProductText"]/text()').extract_first()
        item['division'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblJobDivisionText"]/text()').extract_first()
        item['job_code'] = response.xpath(
            '//span[@id="ContentPlaceHolder1_JobDetails2_lblJobCodeText"]/text()').extract_first()
        item['posting_date_str'] = self.get_posting_date(id)
        if item['posting_date_str']:
            item['posting_date'] = self.parse_datetime(item['posting_date_str'])
        # print item
        yield item

    def get_posting_date(self, id):
        item = my_mongo.microsoft_tmp_collection.find_one({
            'id': id
        })
        if item:
            return item['posting_date']
        else:
            self.logger.info(u'Can not get date from tmp_collection: id={}'.format(id))
            return None

    def parse_datetime(self, datetime_str):
        try:
            datetime_object = datetime.strptime(datetime_str, '%d %b %Y')
            return datetime_object
        except:
            self.logger.info(u'Can not parse datetime {}'.format(datetime_str))
            return None
