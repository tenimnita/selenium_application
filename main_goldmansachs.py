from datetime import datetime

from goldmansach.spiders.goldmansach_spider import GoldmansachSpider
from scrapy.utils.project import get_project_settings
from scrapy.crawler import CrawlerProcess


if __name__ == '__main__':
    settings = get_project_settings()
    settings.set('LOG_FILE', 'log/goldmansachs/' + datetime.now().strftime('%Y-%m-%d-%H-%M') + '.log')

    process = CrawlerProcess(settings)

    process.crawl(GoldmansachSpider)
    process.start()
